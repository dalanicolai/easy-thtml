;; -*- lexical-binding: t; -*-


;; -*- eval: (outline-minor-mode); -*-

;;; easy-thtml.el --- Write blogs made with thtml by markdown or org-mode

;; Copyright (C) 2020 by Daniel Nicolai

;; Author: Daniel Nicolai
;; URL: https://github.com/dalanicolai/emacs-easy-thtml
;; Version: 1.0
;; Package-Requires: ((emacs "25.1") (popup "0.5.3") (request "0.3.0"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Emacs major mode for writing blogs made with thtml
;; using org-mode source files`'
;; You can publish your blog to github pages.

;;; Code:


;; (require 'cl-lib)
;; (require 'url)
;; (require 'request)

;;; Custom variabels

(defgroup easy-thtml nil
  "Write blogs made with thtml."
  :group 'tools)

(defcustom easy-thtml-projects-alist '(("website" . ("~/git/website" . "make.el"))
                                       ("org-blog" . ("~/git/org-blog" . "make-easy-thtml-with-bulma.el")))
  "Directory of your thtml site (containing conf.py)."
  :group 'easy-thtml
  :type 'directory)

(defcustom easy-thtml-project "website"
  "Directory of your thtml site (containing conf.py)."
  :group 'easy-thtml
  :type 'directory)

(defcustom easy-thtml-default-author (or user-full-name)
  "Default extension when posting new articles."
  :group 'easy-thtml
  :type 'string)

(defcustom easy-thtml-preview-url "http://localhost:8000/"
  "Preview url of `easy-thtml'."
  :group 'easy-thtml
  :type 'string)

(defcustom easy-thtml-url nil
  "Url of the site operated by thtml."
  :group 'easy-thtml
  :type 'string)


(defun easy-thtml-switch-project ()
  (interactive)
  (setq easy-thtml-project
        (completing-read "Select project: "
                         easy-thtml-projects-alist))
  (easy-thtml--load-publish-file))

(defun easy-thtml--project-config (&optional project)
  (alist-get (or project easy-thtml-project)
             easy-thtml-projects-alist
             nil nil 'string=))

;; (defcustom easy-thtml-rootdir (car (easy-thtml-project-config easy-thtml-current-project))
;;   "Directory of your thtml site (containing conf.py)."
;;   :group 'easy-thtml
;;   :type 'directory)

(defun easy-thtml--rootdir (&optional project)
  (car (easy-thtml--project-config (or project easy-thtml-project))))

;; (defcustom easy-thtml-publish-file (cdr (easy-thtml-project-config easy-thtml-current-project))
;;   "Nikola binary."
;;   :group 'easy-thtml
;;   :type 'string)
(defun easy-thtml--publish-file (&optional project)
  (cdr (easy-thtml--project-config (or project easy-thtml-project))))

(defun easy-thtml--load-publish-file (&optional project)
  (let ((project (or project easy-thtml-project))
        (default-directory (print (easy-thtml--rootdir project))))
    (load-file (easy-thtml--publish-file project))))

(easy-thtml--load-publish-file)

(defun easy-thtml-find-publish-file (&optional project)
  (interactive)
  (find-file (concat
              (file-name-as-directory (easy-thtml--rootdir project))
              (easy-thtml--publish-file project))))

(defcustom easy-thtml-default-blog-project "homepage-blog"
  "Directory where the theme stores its posts."
  :group 'easy-thtml
  :type 'string)

(defvar easy-thtml-current-blog-project nil)


(defun easy-thtml--postdir ()
  (plist-get (cdr (assoc
                   (or easy-thtml-current-blog-project
                       easy-thtml-default-blog-project)
                   org-publish-project-alist))
             :base-directory))

(defun easy-thtml-get-posts-alist ()
  (mapcar (lambda (x)
            (let ((index (or (string= x
                                      (concat
                                       (file-name-as-directory
                                        (easy-thtml--postdir))
                                       "index.org"))))
                  (post-slug (car (last (split-string (url-file-directory x) "/") 2))))
              (cons (if index
                        (concat post-slug
                                (propertize " (index)" 'face 'warning))
                      post-slug)
                    x)))
          (directory-files-recursively (easy-thtml--postdir) "org$")))


(defun easy-thtml-public-postdir ()
  (plist-get (cdr (assoc
                   (or easy-thtml-current-blog-project
                       easy-thtml-default-blog-project)
                   org-publish-project-alist))
             :publishing-directory))

(defun easy-thtml-find-post ()
  (interactive)
  (let ((posts (easy-thtml-get-posts-alist)))
    (find-file
     (alist-get (completing-read "Open post: " posts)
                posts nil nil 'string=))))

;; (defcustom easy-thtml-pagedir "page"
;;   "Directory where the theme stores its posts."
;;   :group 'easy-thtml
;;   :type 'directory)


(defun easy-thtml-sluggify (str)
  (replace-regexp-in-string "_\\|--"
                            "-"
                            (replace-regexp-in-string "[^a-z0-9_-]"
                                                      ""
                                                      (mapconcat 'identity
                                                                 (split-string (downcase str))
                                                                 "-"))))

(defun easy-thtml-post-template (title)
  (concat "#+TITLE: " title "\n\n"))

(defun easy-thtml-create-post (title author)
  "Create a new post"
  (interactive (list (read-string "Post title: ")
                     (read-string "Name author: " easy-thtml-default-author)))
  (let* ((default-directory (easy-thtml--postdir))
         (slug (easy-thtml-sluggify title)))
    ;; (make-directory slug)
    (find-file (concat slug "/" "index.org"))
    (insert (easy-thtml-post-template title))))

(defun easy-thtml-view-html ()
  (interactive)
  (if (string= major-mode "org-mode")
      (find-file (concat (file-name-as-directory (easy-thtml-public-postdir))
                         (file-name-as-directory (spacemacs/get-parent-dir))
                       "index.html"))
    (message "function should be called from in org-mode buffer")))

(defun easy-thtml-build ()
  (interactive)
  (let ((default-directory (easy-thtml--rootdir)))
    (let ((enable-local-variables :all)
          (find-file-hook nil))
      (org-publish "homepage" t))
  
  (templated-html-create-sitemap-xml "public/sitemap.xml" "public" templated-html-site-url)))

(defun easy-thtml-preview ()
  (interactive)
  (httpd-serve-directory
   (concat (file-name-as-directory (easy-thtml--rootdir))
           "public/"))
  (if (string= major-mode "org-mode")
      (eaf-open-browser
       (concat "http://localhost:8080/"
                (car
                 (split-string (url-file-directory buffer-file-name)
                               nil
                               nil
                               (file-name-as-directory (expand-file-name (easy-thtml--rootdir)))))
               "index.html"))))

(defun easy-thtml-magit-status ()
  (interactive)
  (magit-status (easy-thtml--rootdir)))

(spacemacs/set-leader-keys "ge" 'magit-status-spacemacs-repo)
(define-derived-mode easy-thtml-mode special-mode "Easy-thtml"
  "Major mode for easy-thtml buffer")

(defun easy-thtml-buffer-name-function ()
  (concat "easy-thtml: " (car (last (split-string (expand-file-name (easy-thtml--rootdir)) "/")))))

(defun easy-thtml-generate-new-buffer ()
  (generate-new-buffer (easy-thtml-buffer-name-function)))

;; TODO finish functions
(defun easy-thtml-setup-buffer-internal ()
  (let ((buffer (easy-thtml-generate-new-buffer)))
    (with-current-buffer buffer
      ;; (insert (propertize "posts" 'face 'font-lock-keyword-face))
      ;; (insert (propertize "posts" 'face 'info-node))
      (insert (propertize "posts" 'face 'org-level-1))
      (newline)
      (mapcar (lambda (x) (insert (car x)) (newline)) (easy-thtml-get-posts-alist)))
    buffer))

(defun easy-thtml ()
  (interactive)
  (let ((default-directory (easy-thtml--rootdir)))
    (switch-to-buffer-other-window (easy-thtml-setup-buffer-internal)))
  (goto-char (point-min))
  (next-line)
  (easy-thtml-mode))

(provide 'easy-thtml)

;;; easy-thtml.el ends here
